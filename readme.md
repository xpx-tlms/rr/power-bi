# Power BI
Reference repo for Power BI.

# Reports
![](docs/sales.png)

![](docs/dash.png)

# Videos
- [Beginning Power BI DAX Functions](https://www.youtube.com/watch?v=QJw4HkagVWc&t=5730s)
- [Data Modeling for Power BI](https://www.youtube.com/live/MrLnibFTtbA?feature=share0)

# Notes
- Built with Power BI Desktop Version: 2.116.622.0 64-bit (April 2023)
